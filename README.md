# MicroDebConf

### Ruby/Rails version

For the API is recommended to use:

* Ruby version 2.4.1
```bash
ruby -v
# ruby 2.4.1p111 (2017-03-22 revision 58053) [x86_64-linux]
```

* Rails version 5.0.2
```bash
rails -v
# Rails 5.2.0
```
To install in any OS just follow the steps in this [link](https://gorails.com/setup)

* Database creation
```bash
rails db:create
```

* Database initialization
```bash
rails db:migrate
```

* How to run the test suite


* Create credentials
```bash
EDITOR=vi bin/rails credentials:edit
```

* How to run the API
```bash
rails s
```

* Change between environments
```bash
```

* How to run the API with docker


* Deployment instructions

To deploy in a machine just clone the repository, if you already have it cloned pull from the latest in master and run it with docker. Make sure that docker its installed in the machine.

# Docker

## Install **Docker** and **Docker Compose**
To do the correct installation of docker and docker-compose just follow the tutorial of the official page available in the following link:
* [Docker CE](https://docs.docker.com/)
 * [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
 * [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
 * [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)


* [Docker compose](https://docs.docker.com/compose/install/)


## Development Environment

* **Build**  
Run the following command to build docker
```bash
sudo docker-compose -f dev.yml build
```

* **Drop Database**  
If databases already exists and you need to reset it, run the following command
```bash
sudo docker-compose -f dev.yml run --rm microdebianconf rails db:drop
```

* **Create Database**
```bash
sudo docker-compose -f dev.yml run --rm microdebianconf rails db:create
```

* **Migrate Database**
```bash
sudo docker-compose -f dev.yml run --rm microdebianconf rails db:migrate
```

* **Migrate Database**  
If you want to create fake data into database use
```bash
sudo docker-compose -f dev.yml run --rm microdebianconf rails db:seed
```

* **Run**
```bash
sudo docker-compose -f dev.yml up
```

## Production Environment

* **Build**  
Run the following command to build docker
```bash
sudo docker-compose -f prod.yml build
```

* **Drop Database**  
If databases already exists and you need to reset it, run the following command
```bash
sudo docker-compose -f prod.yml run --rm microdebianconf rails db:drop
```

* **Create Database**
```bash
sudo docker-compose -f prod.yml run --rm microdebianconf rails db:create
```

* **Migrate Database**
```bash
sudo docker-compose -f prod.yml run --rm microdebianconf rails db:migrate
```

* **Migrate Database**  
If you want to create fake data into database use
```bash
sudo docker-compose -f prod.yml run --rm microdebianconf rails db:seed
```

* **Run**
```bash
sudo docker-compose -f prod.yml up
```

